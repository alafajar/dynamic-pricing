import 'antd/dist/antd.css'; 
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";
import { Layout, Menu, Result } from 'antd';

import { HeadToHead } from "./Page/HeadToHead";
import { Competitor } from "./Page/Competitor";
import { FormInput } from "./Page/FormInput";
import CompetitorComparison from './Page/CompetitorComparison';
import SalesData from './Page/SalesData';

const { Header, Content, Footer } = Layout;


// grouping array by key
  function groupByKey(array, key) {
    return array
      .reduce((hash, obj) => {
        if(obj[key] === undefined) return hash; 
        return Object.assign(hash, { [obj[key]]:( hash[obj[key]] || [] ).concat(obj)})
      }, {})
  }

  // random color
  function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color   = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  // calculate table from boxplot
  const getData = (data, value) => {

    //get any percentile from an array
    function getPercentile(data, percentile) {
        data.sort(numSort);
        const arrLength = data.length
        let   index     = (percentile/100) * arrLength;
        let result;

        if (Math.floor(index) === index) { //check even/odds
            result = (data[Math.floor(index-1)] + data[Math.floor(index)])/2;
        } else {
            result = data[Math.floor(index)];
        }
        return result;
    }

    // different price

    //because .sort() doesn't sort numbers correctly
    const numSort = (a,b) =>  (a - b);

    let q1 = 0, q2 = 0, q3 = 0, min=0, max=0;
    if(value === 'boxplot'){
      let result = data.map( (val, idx) => {
          let newObj      = {};
          if(val.name === 'Teguk'){
            q1                     = getPercentile(val.x, 25);
            q2                     = getPercentile(val.x, 50);
            q3                     = getPercentile(val.x, 75);
            min                    = Math.min.apply(Math, val.x);
            max                    = Math.max.apply(Math, val.x);
          }
          
              newObj.competitor = val.name;
              newObj.key        = idx;
              newObj.min        = Math.min.apply(Math, val.x);
              newObj.min        = newObj.min + ` (${Math.round(((newObj.min - min)/min)*100)}%)`;
              newObj.max        = Math.max.apply(Math, val.x);
              newObj.max        = newObj.max + ` (${Math.round(((newObj.max - max)/max)*100)}%)`;
              newObj.q1         = getPercentile(val.x, 25);
              newObj.q1         = newObj.q1 + ` (${Math.round(((newObj.q1 - q1)/q1)*100)}%)`;
              newObj.q2         = getPercentile(val.x, 50);
              newObj.q2         = newObj.q2 + ` (${Math.round(((newObj.q2 - q2)/q2)*100)}%)`;
              newObj.q3         = getPercentile(val.x, 75);
              newObj.q3         = newObj.q3 + ` (${Math.round(((newObj.q3 - q3)/q3)*100)}%)`;
          return newObj;
      });
      return result;
    }

    if(value === 'competitor'){
      let result = data.map( (val, idx) => {
        let newObj      = {};
          newObj.competitor = val.name;
          newObj.key        = idx;
          newObj.min        = Math.min.apply(Math, val.x);
          newObj.max        = Math.max.apply(Math, val.x);
          newObj.q1         = getPercentile(val.x, 25);
          newObj.q2         = getPercentile(val.x, 50);
          newObj.q3         = getPercentile(val.x, 75);
        return newObj;
      });
      return result;
    }
}



function App() {

  return (
    <Layout style={{minHeight:'100vh'}} className="m-auto w-75">
      <BrowserRouter>
      <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" className="text-center">
            <Menu.Item key="2"><Link to="/box-plot-competitor">Competitor Price Comparison</Link></Menu.Item>
            <Menu.Item key="3"><Link to="/competitor">Competitor Price List</Link></Menu.Item>
            <Menu.Item key="4"><Link to="/head-to-head">Product Comparison</Link></Menu.Item>
            <Menu.Item key="5"><Link to="/form">Manual Data Entry</Link></Menu.Item>
            <Menu.Item key="6"><Link to="/sales-data">Upload Sales Data</Link></Menu.Item>
        </Menu>
      </Header>


        <Content className="site-layout" style={{ padding: '0 50px', marginTop: 64 }}>
          <div className="site-layout-background flex justify-center items-center" style={{ padding: 24, minHeight: '80vh' }}>
          <Result
            status   = "403"
            title    = "Dynamic Pricing Dashboard"
            subTitle = "Select the action in navigation"
          />
          <Switch>
            <Route path="/box-plot-competitor">
                  <CompetitorComparison 
                    groupByKey  = {groupByKey}
                    getData     = {getData}
                    randomColor = {getRandomColor}/>
            </Route>

            <Route exact path='/competitor'>
                  <Competitor groupByKey={groupByKey} randomColor={getRandomColor}></Competitor>
            </Route>

          <Route path="/head-to-head">
                  <HeadToHead groupByKey={groupByKey} getRandomColor={getRandomColor}/>
                </Route>
          <Route path="/form">
              <FormInput/>
          </Route>

          <Route path="/sales-data">
              <SalesData groupByKey={groupByKey} getRandomColor={getRandomColor}/>
          </Route>
          
          </Switch>
          </div>
        </Content>

      <Footer style={{ textAlign: 'center' }}>Dynamic Pricing ©2021 Created by Team</Footer>
  </BrowserRouter>
  </Layout>
  );
}

export default App;

