import axios from 'axios';
import React, {useEffect, useState, useRef} from 'react';
import { Select, Button, Table, Space, Spin, message } from 'antd';
import Plot from 'react-plotly.js';
import moment from 'moment';


const { Option } = Select;

const layout = {
            title   : 'Grouped Competitor Box Plot',
            boxmode : 'group',
            autosize: true,
            xaxis   : {
                zeroline: false
            },
        };

const THCOL = [
    {key:0, dataIndex: 'competitor', title: 'Competitor'},
    {key:1, dataIndex: 'min', title: 'Minimum'},
    {key:2, dataIndex: 'q1', title: 'Quartile 1'},
    {key:3, dataIndex: 'q2', title: 'Quartile 2'},
    {key:4, dataIndex: 'q3', title: 'Quartile 3'},
    {key:5, dataIndex: 'max', title: 'Maximum'},
]

// documentation
// https://plotly.com/javascript/box-plots/
// https://plotly.com/javascript/react/
// https://bobaprice.herokuapp.com/docs/
export const BoxPlot  = ({groupByKey, randomColor, dataAll, getData}) => {
    const [category, setCategory]           = useState([]);
    const [company, setCompany]             = useState([]);
    const [filterTime, setFilterTime]       = useState([]);
    const [subCategory, setSubCategory]     = useState('');
    const [subTime, setSubTime]             = useState('');
    const [hasData, setHasData]             = useState(false);
    const [boxData, setBoxData]             = useState([]);
    const [thCol, setThCol]                 = useState([]);
    const [data, setData]                   = useState([]);
    const [buttonLoading, setButtonLoading] = useState(false);
    const [isLoading, setIsLoading]         = useState(false);
    const [isTable, setIstable]             = useState(false);
    const isMounted                         = useRef(true);

    useEffect( () => {
        isMounted.current = true;
        if(isMounted.current){
            const fetchData =  () => {
                try {
                        const response      = dataAll;
                        const result        = response.result;
                        const resultComp    = groupByKey(result, 'company');
                        const arrComp       = Object.keys(resultComp);
                        const groupCategory = Object.keys(groupByKey(result, 'category'));
    
                        const formatedPrice = resultComp['Teguk'].map(item => item.price).flat()
    
                        const formatedDate  = resultComp['Teguk'].map(item =>{ 
                            return subTime === '' ? item.date = 'Teguk' : item.date = subTime;
                        }).flat();
    
                        if(response.status === 200){
                            setIsLoading(false);
                            setCategory(groupCategory);
                            setCompany(arrComp);
                            setFilterTime(['today', 'this week', 'this month']);
                            setBoxData([{
                                x          : formatedPrice,
                                y          : formatedDate,
                                name       : 'Teguk',
                                text       : 'Teguk',
                                hoverinfo  : 'Teguk',
                                boxpoints  : false,
                                marker     : {color: randomColor()},
                                type       : 'box',
                                boxmean    : false,
                                orientation: 'h'
                            }]);
                            
                        }                    
                } catch (err) {
                    console.log( err.response);
                return false;

                }
            }
            fetchData();
            return () => {
                isMounted.current = false;
            };
        }

    }, [groupByKey, randomColor, subTime, dataAll]);

    useEffect( () => {
        const today = moment().format('YYYY-MM-DD');
        const week  = moment().subtract(7, 'days').format('YYYY-MM-DD');
        const month = moment().subtract(1, 'months').format('YYYY-MM-DD');

        if(hasData){
            let dataParam = {start_date: '', end_date: ''}
            
            if( subTime === 'today'){
                dataParam = {start_date: today}
                delete dataParam.end_date;
                
            } else if( subTime === 'this week'){
                dataParam = {start_date: week, end_date: today}
                
            } else {
                dataParam = {start_date: month, end_date: today}
            }

            axios.get(`${process.env.REACT_APP_URL_PRODUCTS}`, {
                            headers: {
                                'Content-Type': 'application/json',
                                'Accept'        : 'application/json',
                                'Authorization' : `${process.env.REACT_APP_AUTH}`
                            }, 
                            params: dataParam,
            }).then( res => {
                const result           = res.data.result;
                const filteredFormated = result.filter(item => {
                    let data = company.filter( val => val === item.company);
                    return item.category === subCategory && item.company === data[0];
                });

                // teguk
                const filterTeguk = result.filter(item => item.company === 'Teguk').map( val =>  ( {...val, date : subTime }));
                const priceTeguk = filterTeguk.map(item => item.price).flat();
                const dateTeguk  = filterTeguk.map(item => item.date = subTime).flat();

                const tegukObj   = {
                    x          : priceTeguk,
                    y          : dateTeguk,
                    name       : 'Teguk',
                    text       : 'Teguk',
                    hoverinfo  : 'Teguk',
                    boxpoints  : false,
                    marker     : {color: randomColor()},
                    type       : 'box',
                    boxmean    : false,
                    orientation: 'h'
                }

                let newArr = [tegukObj];

                // data for boxplot
                for (let index = 0; index < company.length; index++) {
                    const val  = company[index];
                    const data = filteredFormated.filter( val2 => val2.company === val );

                    if( data.length > 0 && val !== 'Teguk'){
                        // dari sini diambil price dan tanggalnya
                        const formatedPrice = data.map(item => item.price).flat()
                        const formatedDate  = data.map(item => item.date = subTime).flat();

                        let filterData = {};

                        filterData = {
                            x          : formatedPrice,
                            y          : formatedDate,
                            name       : val,
                            text       : val,
                            hoverinfo  : val,
                            boxpoints  : false,
                            marker     : {color: randomColor()},
                            type       : 'box',
                            boxmean    : false,
                            orientation: 'h',
                        }

                        // newTd.push(dataStat);
                        newArr.push(filterData);
                    }
                };

                // get data boxplot
                setData(getData(newArr, 'boxplot'));

                setThCol(THCOL);     
                setBoxData(newArr);
                setHasData(false);
                setIstable(true);
                setButtonLoading(false);
            });
        }

    }, [hasData, company, randomColor, subCategory, subTime, isTable, getData, setButtonLoading]);

    const changeCategory = (value) => setSubCategory(value);
    const changeTime     = (value) => setSubTime(value);

    const clickSubmit    = (e) => {
        e.preventDefault();
        if(subCategory !== '' && subTime !== ''){
            setHasData(true);
            setButtonLoading(true);
        } else message.error('make sure select the options');
    }

    return (
        <div>
            <h2 className="text-5xl mb-12 text-center">Competitor Price Summary</h2>

            {!isLoading ? 
            <>
            <div className="group-select flex justify-center">
                
                <Select 
                    defaultValue = "Category"
                    style        = {{ width: 240 }}
                    className    = "mr-4 text-2xl"
                    onChange     = {(val) => changeCategory(val)}>
                        {category && category.map( (val,key) => {
                            return <Option value={val} key={key}>{val}</Option>
                        })}
                </Select>

                <Select 
                    defaultValue = "select time"
                    style        = {{ width: 240 }}
                    className    = "mr-4 text-2xl"
                    onChange     = {(val) => changeTime(val)}>
                        {filterTime && filterTime.map( (val, key) => {
                            return <Option value={val} key={key}>{val}</Option>
                        })}
                </Select>

                <Button 
                    type    = "primary"
                    onClick = {(e) => clickSubmit(e)}
                    loading = {buttonLoading}
                >submit</Button>
            </div>
            
            <Plot
                style      = {{display:'block'}}
                data       = {boxData}
                responsive = {true}
                layout     = {layout}/>
            {isTable ? 
            <>
                <h2    className = "my-8">Table Box Plot</h2>
                <Table columns   = {thCol} dataSource = {data} scroll = {{ y: 600 }} /> 
            </>
            : ''}
            </>
            : <Space size="middle" className="my-4 flex justify-center"><Spin tip="loading data" size="large"/></Space>
            }
        </div>
    )
}


