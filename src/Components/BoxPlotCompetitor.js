import axios from 'axios';
import React, {useEffect, useState, useRef} from 'react';
import { Select, Button, Table, Space, Spin, message } from 'antd';
import Plot from 'react-plotly.js';
import moment from 'moment';

const { Option } = Select;

const layout = {
    title   : 'Grouped Competitor Box Plot',
    boxmode : 'group',
    autosize: true,
    xaxis   : {
        zeroline: false
    },
};

const groupTimes = ['today', 'this week', 'this month'];

const THCOL = [
    {key:0, dataIndex: 'competitor', title: 'Competitor'},
    {key:1, dataIndex: 'min', title: 'Minimum'},
    {key:2, dataIndex: 'q1', title: 'Quartile 1'},
    {key:3, dataIndex: 'q2', title: 'Quartile 2'},
    {key:4, dataIndex: 'q3', title: 'Quartile 3'},
    {key:5, dataIndex: 'max', title: 'Maximum'},
]

// documentation
// https://plotly.com/javascript/box-plots/
// https://plotly.com/javascript/react/
export const BoxPlotCompetitor = ({groupByKey, randomColor, dataAll, getData}) => {
    
    const [compe, setCompe]                 = useState([]);
    const [times, setTimes]                 = useState([]);
    const [resetCompe, setResetCompe]       = useState([]);
    const [hasData, setHasData]             = useState(false);
    const [selectTime, setSelectTime]       = useState('');
    const [selectCompe1, setSelectCompe1]   = useState('');
    const [selectCompe2, setSelectCompe2]   = useState('');
    const [boxPlot, setBoxPlot]             = useState([]);
    const [thCol, setThCol]                 = useState([]);
    const [data, setData]                   = useState([]);
    const [buttonLoading, setButtonLoading] = useState(false);
    const [isTable, setIstable]             = useState(false);
    const isMounted                         = useRef(true);

    useEffect(() => {
        isMounted.current = true;
        if(isMounted.current){
            
            const fetchData = () => {
                try {
                    const response = dataAll;
                    const result   = response.result;

                    //for competitor
                    const groupCompe = groupByKey(result, 'company');
                    const keyCompe   = Object.keys(groupCompe);

                    if(response.status === 200){
                        setTimes(groupTimes);
                        setResetCompe(keyCompe);
                        setCompe(keyCompe);
                    }

                } catch (error) {
                    console.log('err', error)
                }
            }
            fetchData();
            return () => {
                isMounted.current = false;
            }
        }
    }, [groupByKey, dataAll]);

    useEffect(() => {
        const today = moment().format('YYYY-MM-DD');
        const week  = moment().subtract(7, 'days').format('YYYY-MM-DD');
        const month = moment().subtract(1, 'months').format('YYYY-MM-DD');

        if(hasData) {
            let dataParam = {start_date: '', end_date: ''}
            
            if( selectTime === 'today'){
                dataParam = {start_date: today}
                delete dataParam.end_date;
                
            } else if( selectTime === 'this week'){
                dataParam = {start_date: week, end_date: today}
                
            } else {
                dataParam = {start_date: month, end_date: today}
            }

            axios.get(`${process.env.REACT_APP_URL_PRODUCTS}`, {
                            headers: {
                                'Content-Type': 'application/json',
                                'Accept'        : 'application/json',
                                'Authorization' : `${process.env.REACT_APP_AUTH}`
                            }, 
                            params: dataParam,
            }).then( res => {
                const result   = res.data.result;
                const mergeArr = [];

                // filter to get only company
                const filterCompe1 = result.filter( item => item.company === selectCompe1).map(val => ({...val, date : selectTime}));
                const filterCompe2 = result.filter( item => item.company === selectCompe2).map(val => ({...val, date : selectTime}));
            
                const formatedPrice1 = filterCompe1.map(item => item.price).flat();
                const formatedDate1  = filterCompe1.map(item => item.date).flat();
                const formatedPrice2 = filterCompe2.map(item => item.price).flat();
                const formatedDate2  = filterCompe2.map(item => item.date).flat();

                compe.map( val => {
                    if(val === selectCompe1){
                        let filterData = {}
                        filterData = {
                            x          : formatedPrice1,
                            y          : formatedDate1,
                            name       : val,
                            text       : val,
                            hoverinfo  : val,
                            boxpoints  : false,
                            marker     : {color: randomColor()},
                            type       : 'box',
                            boxmean    : false,
                            orientation: 'h',
                        }
                        mergeArr.push(filterData);
                    }

                    if(val === selectCompe2){
                        let filterData = {}
                        filterData = {
                            x          : formatedPrice2,
                            y          : formatedDate2,
                            name       : val,
                            text       : val,
                            hoverinfo  : val,
                            boxpoints  : false,
                            marker     : {color: randomColor()},
                            type       : 'box',
                            boxmean    : false,
                            orientation: 'h',
                        }
                        mergeArr.push(filterData)
                    }

                    return mergeArr;
                });

                // get data boxplot table
                setData(getData(mergeArr, 'competitor'));
                setThCol(THCOL);  

                setBoxPlot(mergeArr);
                setHasData(true);
                setIstable(true);
                setButtonLoading(false);
                
            }).catch( err => console.log('error', err))
        }
        
        return () => {
            
        }
    }, [hasData, compe, randomColor, selectCompe1, selectCompe2, selectTime, getData])

    
    const changeTime = value => setSelectTime(value);

    const changeCompetitor1 = value => {
        const newArr = compe.filter(val => val !== value);
        setCompe(newArr);
        setSelectCompe1(value);
    }
    const changeCompetitor2 = value =>  {
        const newArr = compe.filter(val => val !== value);
        setCompe(newArr);
        setSelectCompe2(value);
    }

    const clickSubmit    = (e) => {
        e.preventDefault();
        if(selectCompe1 !== '' && selectCompe2 !== '' && selectTime !== ''){
            setCompe(resetCompe);
            setHasData(true);
            setButtonLoading(true);
        } else message.error('make sure select the options');
    }

    return (
        <div>
            <h2 className="text-5xl mb-12 text-center">Competitor Price Comparison</h2>

            <div className="group-select flex justify-center">
                
                <Select 
                    defaultValue = "Competitor 1"
                    style        = {{ width: 240 }}
                    className    = "mr-4 text-2xl"
                    onChange     = {(val) => changeCompetitor1(val)}>
                        {compe && compe.map( (val,key) => {
                            return <Option value={val} key={key}>{val}</Option>
                        })}
                </Select>

                <Select 
                    defaultValue = "Competitor 2"
                    style        = {{ width: 240 }}
                    className    = "mr-4 text-2xl"
                    onChange     = {(val) => changeCompetitor2(val)}>
                        {compe && compe.map( (val,key) => {
                            return <Option value={val} key={key}>{val}</Option>
                        })}
                </Select>

                <Select 
                    defaultValue = "Select time"
                    style        = {{ width: 240 }}
                    className    = "mr-4 text-2xl"
                    onChange     = {(val) => changeTime(val)}>
                        {times && times.map( (val, key) => {
                            return <Option value={val} key={key}>{val}</Option>
                        })}
                </Select>

                <Button 
                    type    = "primary"
                    onClick = {(e) => clickSubmit(e)}
                    loading = {buttonLoading}
                >submit</Button>
            </div>

            {hasData ? 
                <>
                <Plot
                style      = {{display:'block'}}
                data       = {boxPlot}
                responsive = {true}
                layout     = {layout}/>
                {isTable ? 
                <>
                    <h2    className = "my-8">Table Box Plot</h2>
                    <Table 
                        columns    = {thCol}
                        dataSource = {data}
                        scroll     = {{ y: 600 }} />
                </>
                : ''}
                    
                </>
            : <Space size="middle" className="my-4 flex justify-center"><Spin tip="loading the action data" size="large"/></Space>}
        </div>
    )
}