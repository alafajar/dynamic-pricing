import React, { useState, useEffect, useRef} from 'react';
import axios from 'axios';
import { Button, message, Select } from 'antd';
import { BarChart, Bar, XAxis, YAxis, Cell, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


const { Option } = Select;

function GraphicSales({  dataTable, groupByKey, getRandomColor }) {
    const [products, setProducts]     = useState([]);
    const [product, setProduct]       = useState('');
    const [dataSource, setDataSource] = useState([]);
    const [btnLoading, setBtnLoading] = useState(false);
    const isMount                     = useRef(true);
    
    // products name
    useEffect(() => {
        isMount.current = true;

        if(isMount.current){
            const fetchData = async () => {
                try {
                    const response = await axios.get(`${process.env.REACT_APP_URL_TARGET}`, {
                        headers: {
                            'Content-Type' : 'application/json',
                            'Accept'       : 'application/json',
                            'Authorization': `${process.env.REACT_APP_AUTH}`
                        }
                    });
                    const result = response.data.result;

                    if(response.status === 200 && Array.isArray(result)){
                        setProducts(result);
                    }
                    
                } catch (error) {
                    message.error(error)
                }
            }
            fetchData();
            return () => {
                isMount.current = false
            }
        }
    }, []);


    // table
    useEffect(() => {
        isMount.current = true;
        if(isMount.current && btnLoading){
            const data = dataTable.length > 0 ? [...dataTable] : [];

            if(data.length === 0 ) message.warn('wait the data');

            const unique = [];
            //remove duplicate data
            data.map(x => unique.filter(a => a.price === x.price && a.sales === x.sales).length > 0 ? null : unique.push(x));
            //filter by product
            const perProduct = unique.filter(val => val.name === product).map((val2, idx )=> {
                let obj = {}
                obj= {
                    key     : idx,
                    source  : val2.source,
                    price   : val2.price,
                    avgSales: val2.sales
                }
                return obj
            })
            // sort online offline
            perProduct.sort((a, b) => b.source < a.source ? -1 : b.source > a.source ? 1 : 0)
            setDataSource(perProduct)
            setBtnLoading(false);
            
            return () => {
                isMount.current = false;
            }
        }

    }, [dataTable, groupByKey, btnLoading, product])


    const clickSubmit = (e) => {
        e.preventDefault();
        setBtnLoading(true)
    }

    const changeProduct = (val) => setProduct(val)

    const tableContent = () => {
        let counter = dataSource.reduce(function(obj, v) {
            obj[v.source] = (obj[v.source] || 0) + 1;
            return obj;
        }, {})

        return dataSource.map((el, index) => {
            return(
                <tr key={el.key} className="ant-table-row">
                    {
                        el.source === 'Online' && index === 0 ?
                        <td className="ant-table-cell" rowSpan={el.source === 'Online' ? counter.Online : counter.Offline}>{el.source}</td>
                        : el.source === 'Offline' && index === counter.Online ?
                        <td className="ant-table-cell" style={{borderTop: `${counter.Offline ? '1px solid red' : 'unset'}`}} rowSpan={el.source === 'Offline' ? counter.Offline : counter.Online}>{el.source}</td>
                        : null
                    }
                    <td style={{borderTop: `${index === counter.Online ? '1px solid red' : 'unset'}`}} >{el.price}</td>
                    <td style={{borderTop: `${index === counter.Online ? '1px solid red' : 'unset'}`}}>{el.avgSales}</td>
                </tr>
            )
        })
    }

    const tickFormatter = () => '';
    const renderTick    = (tickProps) => {
        const { x, y, payload } = tickProps;
        const { value }         = payload;

        let counter = dataSource.reduce(function(obj, v) {
            obj[v.source] = (obj[v.source] || 0) + 1;
            return obj;
        }, {})

        if(payload.index === (counter.Online)/2 && value === 'Online'){
            return <text x={x} y={y + 15} textAnchor="middle">Online</text>;
        }

        if(payload.index === (counter.Online + (counter.Offline/2)) && value === 'Offline'){
            return <text x={x} y={y + 15} textAnchor="middle">Offline</text>;
        }
        
        return null
    }

    const editCell = () => {
        let random1 = getRandomColor();
        let random2 = getRandomColor();

        return dataSource.map((val, idx) => {
            if(val.source === 'Online'){
                return <Cell key={`cell-${idx}`} fill={random1}/>
            }
            if(val.source === 'Offline'){
                return <Cell key={`cell-${idx}`} fill={random2}/>
            }
            
            return val
        })         
    }
    
    return (
        <div className="pb-12">
            <div className="group-select flex justify-center mb-8">
                <Select 
                    defaultValue = "Category"
                    style        = {{ width: 240 }}
                    className    = "mr-4 text-2xl"
                    onChange     = {(val) => changeProduct(val)}>
                        {products && products.map( (val,key) => {
                            return <Option value={val.name} key={key}>{val.name}</Option>
                        })}
                </Select>

                <Button 
                    type    = "primary"
                    onClick = {(e) => clickSubmit(e)}
                    loading = {btnLoading}
                >submit</Button>
            </div>

            {/* table */}
            <div className="ant-table ant-table-fixed-header">
                <div className="ant-table-container">
                    <div className="ant-table-header" style={{overflow:'hidden'}}>
                        <table style={{tableLayout: 'fixed'}}>
                            <thead className="ant-table-thead">
                                <tr>
                                    <th className="ant-table-cell">Source</th>
                                    <th className="ant-table-cell">Price</th>
                                    <th className="ant-table-cell">Average Of Sales</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div className="ant-table-body" style={{overflowY: 'scroll', maxHeight:'50rem'}}>
                        <table style={{tableLayout: 'fixed'}}>
                            <tbody className="ant-table-tbody">
                                {dataSource.length > 1 ? tableContent() : null}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {/* graphic bar */}
            <ResponsiveContainer width="95%" height={400}>
                <BarChart
                width   = {900}
                height  = {300}
                data    = {dataSource}
                margin  = {{
                    top   : 5,
                    right : 30,
                    left  : 20,
                    bottom: 5,
                }}
                barGap="10%"
                >
                <CartesianGrid strokeDasharray="3 3" />
                
                <XAxis
                    dataKey       = "source"
                    axisLine      = {false}
                    tickLine      = {false}
                    interval      = {0}
                    height        = {1}
                    tickFormatter = {tickFormatter}
                    tick          = {renderTick}
                />
                
                <YAxis/>
                <Tooltip />
                <Legend wrapperStyle={{bottom:'-2rem'}}/>
                <Bar dataKey="price">
                    {editCell()}
                </Bar>
                </BarChart>
            </ResponsiveContainer>
        </div>
    )
}

export default GraphicSales;


