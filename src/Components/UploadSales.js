import React, { useState, useEffect, useRef} from 'react';
import moment from 'moment';
import axios from 'axios';
import { Upload, Form, Button, message, List } from 'antd';
import readXlsxFile from 'read-excel-file';
import { UploadOutlined } from '@ant-design/icons';

const schema = {
    'Date'        : {prop: 'date', type: val => moment(new Date(val)).format('YYYY-MM-DD HH:mm:ss')},
    'Product Name': { prop: 'name', type: String},
    'Target ID'   : { prop: 'target', type: String },
    'Sales'       : { prop: 'sales', type: Number },
    'Source'      : { prop: 'source', type: String }
}

const layout = {
    labelCol  : { span: 24 },
    labelAlign: 'left',
    wrapperCol: { span: 24 },
};

const tailLayout = { wrapperCol: { span: 24 } };

const listFormat = [
    'Format File : Excel (.xlsx)',
    'Data tercantum di Sheet1.',
    'Kolom: Date (Date), Product Name (Text), Target ID (Number), Sales (Number), Source (Text).',
];

function UploadSales({ setRow }) {
    const fileInput                   = useRef();
    const isMount                     = useRef(true);
    const [rows, setrows]             = useState([]);
    const [hasLoading, setHasLoading] = useState(false);
    const [hasData, setHasData]       = useState(false);

    useEffect(() => {
        isMount.current = true;
        if(hasData && hasLoading && isMount.current){
            axios.post(`${process.env.REACT_APP_URL_SALES}`, rows, {
                headers: {
                    "Access-Control-Allow-Headers": "AuthorizationX-Requested-With",
                    "Access-Control-Allow-Origin" : "*",
                    'Content-Type'                : 'application/json',
                    'accept'                      : 'application/json',
                    'Authorization'               : `${process.env.REACT_APP_AUTH}`
                }
            }).then(response => {
                if(response.data.status === 200){
                    message.success(response.data.result.message)
                    setRow(rows);       
                    setHasData([]);
                    setHasLoading(false);                    
                }

            }).catch(err => {
                message.error(err)
            })
            
            return () => {
                isMount.current = false;
            }
        }
    }, [hasData, hasLoading, rows, setRow])

    const onFinish = (values) => {
        if(values.upload !== undefined && Array.isArray(values.upload)){
            const keys = ['date', 'name', 'sales', 'source', 'target'];

            const result = rows.map(val => {
                const objKeys = Object.keys(val);
                if(objKeys.length !== keys.length) message.error('invalid data format');
                const checkResult = objKeys.map(val2 => val === val2 ? true : false)
                return checkResult ? val : [];
            });
            
            if(result.length <= 0 || result.length === -1) message.error('check your data specific format')
            
            setHasData(true);
            setHasLoading(true);
        }
    };

    const onFinishFailed = (errorInfo ) => {
        message.error(errorInfo)
    };

    const normFile = (e) => {
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };

    const beforeUpload = (file, fileList) => {

        const fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        if(file.type !== fileType) {
            message.error('wrong data format or extention format')
            return false;
        };

        readXlsxFile(file, { schema }).then(({ rows, errors }) => {
            setrows(rows);
        })

        return (rows.length > 0) ? true : false;
    }

    return (
        <div>
            <h2 className="capitalize text-center text-3xl font-display font-semibold">Upload Sales Data</h2>
            <List
                bordered
                size       = "small"
                header     = {<div><b>Format Data Excel</b></div>}
                dataSource = {listFormat}
                renderItem = {item => <List.Item>{item}</List.Item>}
            />
            <div className="group-select flex justify-center my-12">
                <Form
                    {...layout}
                    name           = "basic"
                    initialValues  = {{ remember: true }}
                    onFinish       = {onFinish}
                    onFinishFailed = {onFinishFailed}
                    className      = 'text-center'
                >
                    <Form.Item
                        name              = "upload"
                        valuePropName     = "fileList"
                        getValueFromEvent = {normFile}
                        extra             = "only accept file excel and right format"
                    >
                        <Upload 
                            name         = "file upload xlx"
                            listType     = "text"
                            ref          = {fileInput}
                            beforeUpload = {beforeUpload}
                            maxCount     = {1}
                        >
                            <Button icon = {<UploadOutlined />}>Click to upload</Button>
                        </Upload>
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit" loading={hasLoading}>
                        Upload Excel
                        </Button>
                    </Form.Item>
                </Form>

                
            </div>
        </div>
    )
}


export default UploadSales
