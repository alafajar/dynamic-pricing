import React, {useEffect, useState, useRef} from 'react';
import axios from 'axios';
import { Select, Button, Table, Space, Spin, message, Result } from 'antd';
import moment from 'moment';
import { CSVLink } from "react-csv";


const { Option } = Select;


// Documentation
//https://ant.design/components/table/
//https://recharts.org/en-US/api/LineChart
export const Competitor = ({groupByKey, randomColor}) => {
    const [arrTime, setArrTime]                   = useState([])
    const [arrCompetitor, setArrCompetitor]       = useState([])
    const [selectTime, setSelectTime]             = useState('');
    const [selectCompetitor, setSelectCompetitor] = useState('');
    const [hasLoading, setHasLoading]             = useState(false);
    const [thCol, setThCol]                       = useState([]);
    const [hasData, setHasData]                   = useState(false);
    const [data, setData]                         = useState([]);
    const [hasContent, setHasContent]             = useState(false);
    const [buttonLoading, setButtonLoading]       = useState(false);
    const isMounted                               = useRef(true);


    useEffect(() => {
        if (isMounted.current) {
            
            message.warning({
                content  : 'waiting load data',
                className: 'custom-class',
                style    : {
                    display       : 'flex',
                    justifyContent: 'center',
                    alignItems    : 'center'
                },
            });
            const fetchData = async () => {
                try {
                    const response = await axios.get(`${process.env.REACT_APP_URL_PRODUCTS}`, {
                                    headers: {
                                        'Content-Type': 'application/json',
                                        'Accept'        : 'application/json',
                                        'Authorization' : `${process.env.REACT_APP_AUTH}`
                                    },
                                    params: { limit: 5000 }
                                });
                    const result     = response.data.result;
    
                    // initialize company
                    const byCompany  = groupByKey(result, 'company');
                    const arrCompany = Object.keys(byCompany); //key company
    
                    // source
                    const bySource  = groupByKey(result, 'source');
                    const arrSource = Object.keys(bySource).map((val) => Object.assign({dataIndex: val, title:val}) );
                    const thSource  = [{
                            dataIndex     : 'competitor',
                            title         : 'competitor',
                        }].concat(arrSource);
    
    
                    if(response.status === 200){
                        message.success({
                            content  : 'data is ready',
                            className: 'custom-class',
                            style    : {
                                display       : 'flex',
                                justifyContent: 'center',
                                alignItems    : 'center'
                            },
                        });
                        setHasLoading(true);
                        setArrTime(['today', 'this week', 'this month']);
                        setThCol(thSource);
                        setArrCompetitor(arrCompany);
                    }
                    
                } catch (err) {
                    console.log('err',err)
                }
            }
            fetchData();
            return () => {
                isMounted.current = false;
            }
        }
    }, [groupByKey]);


    useEffect(()=> {
        const today = moment().format('YYYY-MM-DD');
        const week  = moment().subtract(7, 'days').format('YYYY-MM-DD');
        const month = moment().subtract(1, 'months').format('YYYY-MM-DD');
        
        if(hasData){

            let dataParam = {start_date: '', end_date: '', limit:7500}
            
            if( selectTime === 'today'){
                dataParam = {start_date: today }
                delete dataParam.end_date;
                
            } else if( selectTime === 'this week'){
                dataParam = {start_date: week, end_date: today}
                
            } else {
                dataParam = {start_date: month, end_date: today}
            }

            axios.get(`${process.env.REACT_APP_URL_PRODUCTS}`, {
                            headers: {
                                'Content-Type': 'application/json',
                                'Accept'        : 'application/json',
                                'Authorization' : `${process.env.REACT_APP_AUTH}`
                            }, 
                            params: dataParam,
            }).then( res => {
                const result     = res.data.result;
                console.log('result', result.length);
                if(result.length === 0) {
                    message.warning({
                        content  : 'oops! data period is empty',
                        className: 'custom-class',
                        style    : {
                            display       : 'flex',
                            justifyContent: 'center',
                            alignItems    : 'center'
                        },
                    });
                    setButtonLoading(false);   
                    setHasContent(true);
                    setHasData(false);
                    setButtonLoading(false);                 
                } else{

                    // group
                    const byCompany  = groupByKey(result, 'company');
                    
                    const arrCompany = Object.keys(byCompany); //key company
                    let arrResult  = [];
                    
                    // filtering array
                    for (let index = 0; index < arrCompany.length; index++) {
                        const element = arrCompany[index];
                    
                        // filter array by branch and price
                        const filteredArr = byCompany[element].reduce((thing, current) => {
                            const x = thing.find(item => item.branch === current.branch && item.price === current.price) ;
                                if (!x) { return thing.concat([current]);} 
                                else { return thing; }
                        }, []);
                        const byFilterCompany = groupByKey(filteredArr, 'company');
                        arrResult.push(byFilterCompany);
                    }
    
                    // filter match with select competitor
                    arrResult = arrResult.filter((val, idx)=> {
                        const element = arrResult[idx];
                        const key     = Object.keys(element)
                        return key.includes(selectCompetitor);
                    });
    
                    arrResult = arrResult[0][selectCompetitor].map( (val, idx) => {
                            let newObj = {}
                                newObj = {
                                    key            : idx,
                                    competitor     : val.name,
                                    'official site': val.source === 'official site' ? `${val.price}`: '',
                                    gofood         : val.source === 'gofood' ? `${val.price}`       : '',
                                    foodspot       : val.source === 'foodspot' ? `${val.price}`     : '',
                                    grabfood       : val.source === 'grabfood' ? `${val.price}`     : '',
                                }
                            return newObj;
                    })
    
                    const unique = [...new Map(arrResult.map(item => [item['competitor'], item])).values()].sort((a, b) => (a.competitor > b.competitor && 1) || -1)
    
                    if(res.status === 200){
                        setData(unique);
                        setHasContent(true);
                        setHasData(false);
                        setButtonLoading(false);
                    }
                }
                
            }).catch(err => {
                console.log('err', err)
            })
        }
    },[hasData, groupByKey, selectCompetitor, selectTime])
    

    const changeCompetitor = (value) => setSelectCompetitor(value);
    const changeTime       = (value) => setSelectTime(value);
    const clickSubmit      = (e) => {
        e.preventDefault();
        setHasData(true);
        setButtonLoading(true);
    };
    
    return (
        <div className="centering-element shadow-xl font-display text-3xl capitalize">
            <h2 className="text-center text-capitalize">Competitor Price List</h2>
            {hasLoading ? 
            <>
                <div className="group-select flex justify-center my-8">
                    <Select 
                        defaultValue = "Group Competitor"
                        style        = {{ width: 240 }}
                        className    = "mr-4 text-2xl"
                        onChange     = {(val) => changeCompetitor(val)}
                        >
                            {arrCompetitor && arrCompetitor.map( (val,key) => {
                                return <Option value={val} key={key}>{val}</Option>
                            })}
                    </Select>

                    <Select 
                        defaultValue = "select time"
                        style        = {{ width: 240 }}
                        className    = "mr-4 text-2xl"
                        onChange     = {(val) => changeTime(val)}
                        >
                            {arrTime && arrTime.map( (val, key) => {
                                return <Option value={val} key={key}>{val}</Option>
                            })}
                    </Select>

                    <Button 
                        type    = "primary"
                        loading = {buttonLoading}
                        onClick = {(e) => clickSubmit(e)}
                    >submit</Button>
                </div>
                {hasContent ? 
                    <>
                        <CSVLink data={data} filename={`${selectCompetitor} - ${selectTime}.csv`} className="hrefStyle block w-1/5 text-center py-6 ml-auto mb-8">Download CSV</CSVLink>
                        <Table columns={thCol} dataSource={data} pagination={{ pageSize: 10 }} scroll={{ y: 600 }} />
                    </>
                : <Result
                        status   = "500"
                        title    = "make sure you select it"
                        subTitle = "Waiting next action"
                    />}
            </>
                : <Space size="middle" className="my-4 flex justify-center"><Spin tip="loading data" size="large"/></Space>
            }
        </div>
    )
}