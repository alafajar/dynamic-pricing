import React, {useState, useEffect, useRef} from 'react'
import axios from 'axios';
import {BoxPlot} from '../Components/BoxPlot'
import moment from 'moment';
import { BoxPlotCompetitor } from '../Components/BoxPlotCompetitor'
import { message } from 'antd';


const CompetitorComparison = ({groupByKey, randomColor, getData}) => {
    const [dataAll, setDataAll] = useState({});
    const [hasData, setHasData] = useState(true);
    const isMounted             = useRef(true);

    const fetchData =  async() => {
        let   dataParam = {limit:5000}
        const response  = await axios.get(`${process.env.REACT_APP_URL_PRODUCTS}`, {
            headers: {
                'Content-Type': 'application/json',
                'Accept'        : 'application/json',
                'Authorization' : `${process.env.REACT_APP_AUTH}`
            },
            params: dataParam,
        });

        if(response.status === 200){
            message.success({
                content  : 'data is ready',
                className: 'custom-class',
                style    : {
                    display       : 'flex',
                    justifyContent: 'center',
                    alignItems    : 'center'
                },
            });
            if(response.data !== undefined){
                setDataAll(response.data);
                setHasData(false);
            }
        }
    }
    
    useEffect(() => {
        isMounted.current = true
        if(isMounted.current){
            message.warning({
                content  : 'waiting load data',
                className: 'custom-class',
                style    : {
                    display       : 'flex',
                    justifyContent: 'center',
                    alignItems    : 'center'
                },
            });
            
            fetchData();
            return () => {
                isMounted.current = false;
            }
        }
    }, []);

    if(hasData) return <h1>loading...</h1>

    return (

        <div className = "centering-element shadow-xl font-display text-3xl capitalize">
            {dataAll  &&
            <BoxPlot 
                    groupByKey  = {groupByKey}
                    getData     = {getData}
                    dataAll     = {dataAll}
                    randomColor = {randomColor}/>
            }

        <hr className="my-20"></hr>
        {
            dataAll &&
            <BoxPlotCompetitor 
                    groupByKey  = {groupByKey}
                    getData     = {getData}
                    dataAll     = {dataAll}
                    randomColor = {randomColor}
            />
        }
        </div>
    )
}

export default CompetitorComparison
