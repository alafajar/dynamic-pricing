import React, {useEffect, useState, useRef} from 'react';
import axios from 'axios';
import { Select, Button, Input, Form, InputNumber, message,  Spin} from 'antd';

const { Option} = Select

const layout = {
    labelCol: { span: 24 },
    labelAlign: 'left',
    wrapperCol: { span: 24 },
};
const tailLayout = {
    wrapperCol: { span: 24 },
};

const CATEGORIES = ['Middle Player', 'Direct', 'Premium Player']


export const FormInput = () => {
    const [dataPost, setDataPost]     = useState({});
    const isMounted                   = useRef(true);
    const [loadingBtn, setLoadingBtn] = useState(false);
    const [category, setCategory]     = useState('');
    const [loading, setLoading]       = useState(false);
    
    useEffect(() => {
        isMounted.current = true;
        if(isMounted.current && Object.keys(dataPost).length > 0 ){
            const fetchData = async () => {
                try {
                    const response = await axios.post(`${process.env.REACT_APP_URL_PRODUCTS}`, dataPost, {
                                    headers: {
                                        "Access-Control-Allow-Headers": "AuthorizationX-Requested-With",
                                        "Access-Control-Allow-Origin" : "*",
                                        'Content-Type'                : 'application/json',
                                        'accept'                      : 'application/json',
                                        'Authorization'               : `${process.env.REACT_APP_AUTH}`
                                    }
                                });
                    const status      = response.data.status;
                    if(status === 200){
                        message.success({
                            content  : response.data.result.message,
                            className: 'custom-class',
                            style    : {
                                display       : 'flex',
                                justifyContent: 'center',
                                alignItems    : 'center'
                            },
                        });
                        setLoading(false);
                        setLoadingBtn(false);
                    }
                } catch (error) {
                    console.log('error', error);
                }
            }
    
            fetchData();
            return () => {
                isMounted.current = false;
            }
        }
    }, [dataPost, setLoading])
    
    const onFinish = (values) => {
        let newData = {};

        Object.keys(values).map(item => {
            if(values[item] === undefined){
                values[item] = ''
            }
        return newData = values;
        });
        
        setLoading(true);
        setLoadingBtn(true);
        setDataPost({...newData, category});
    };

    const numberOnly = (val) => {
        let reg = /^-?\d*\.?\d*$/;
        return reg.test(val);  // returns a boolean
    }
    
    return (
        <div className="centering-element font-display shadow-xl text-3xl capitalize">
            {loading ? 
            <Spin 
                className = "flex justify-center items-center"
                style     = {{height:'40vh'}} size = "large"
            />
            :
            <div className="group-select flex justify-center">
                <Form
                {...layout}
                name           = "Manual Data Entry"
                initialValues  = {{ remember: true }}
                onFinish       = {onFinish}
                >
                    <Form.Item
                        label = "Product Name"
                        name  = "name"
                        rules = {[{ required: true, 
                                    message: 'Please input your Name of product !',
                                    type   : 'string' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Price Product"
                        name="price"
                        rules={[
                            {
                                required: true,
                                message: 'Wrong format. Number only',
                                pattern: new RegExp(/^-?\d*\.?\d*$/),
                            }
                        ]}
                    >
                        <InputNumber style={{width: '100%'}} onChange={numberOnly} />
                    </Form.Item>

                    <Form.Item
                        label="Image Product (opsional)"
                        name="image"
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item label="Category Product">
                    <Select 
                        defaultValue = "select category"
                        onChange={val => setCategory(val)}
                    > 
                        {CATEGORIES && CATEGORIES.map( (val,key) => {
                            return <Option value={val} key={key}>{val}</Option>
                        })}
                    </Select>
                    </Form.Item>

                    <Form.Item
                        label="Company Product"
                        name="company"
                        rules={[{ required: true, message: 'Please input your company!', type: 'string' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Branch Product"
                        name="branch"
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Address Product"
                        name="address"
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit" loading={loadingBtn}>
                        Submit
                        </Button>
                    </Form.Item>
                </Form>
            
            </div>
            }
        </div>
    )
}
