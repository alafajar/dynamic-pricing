import axios from 'axios';
import React, {useEffect, useState, useRef} from 'react';
import { Select, Button, Table, message } from 'antd';
import moment from 'moment';
import { Bar, BarChart, CartesianGrid, Tooltip, Legend, XAxis, YAxis } from 'recharts';
import DataFrame from 'dataframe-js';

const { Option } = Select;


export const HeadToHead = ({groupByKey, getRandomColor}) => {
    const [products, setProducts]           = useState([]);
    const [product, setProduct]             = useState({});
    const [periods, setPeriods]             = useState([]);
    const [period, setPeriod]               = useState('');
    const [buttonLoading, setButtonLoading] = useState(false);
    const [hasContent, setHasContent]       = useState(false);
    const [THCOL, setTHCOL]                 = useState([]);
    const [tableData, setTableData]         = useState([]);
    const [barTable, setBarTable]           = useState([]);
    const isMount                           = useRef(true);

    useEffect(() => {
        isMount.current = true;
        if(isMount.current){
            const fetchData = async () => {
                try {
                    const response = await axios.get(`${process.env.REACT_APP_URL_TARGET}`, {
                        headers: {
                            'Content-Type' : 'application/json',
                            'Accept'       : 'application/json',
                            'Authorization': `${process.env.REACT_APP_AUTH}`
                        }
                    });
                    const result = response.data.result;
                    if(response.status === 200 && Array.isArray(result)){
                        setProducts(result);
                        setPeriods(['today', 'this week', 'this month']);
                    }

                } catch (error) {
                    message.success({
                        content  : error.error,
                        className: 'custom-class',
                        style    : {
                            display       : 'flex',
                            justifyContent: 'center',
                            alignItems    : 'center'
                        },
                    });
                }
            }

            fetchData();
            return () => {
                isMount.current = false;
            }
        }
    }, []);

    useEffect(() => {
        isMount.current = true;

        const today = moment().format('YYYY-MM-DD');
        const week  = moment().subtract(7, 'days').format('YYYY-MM-DD');
        const month = moment().subtract(1, 'months').format('YYYY-MM-DD');

        if(isMount.current && buttonLoading){
            message.warning('wait the data');

            let dataParam = {start_date: '', end_date: '', target: product.target, limit:5000}
            
            if( period === 'today'){
                dataParam = {...dataParam, start_date: today}
                delete dataParam.end_date;  
            } else if( period === 'this week'){
                dataParam = {...dataParam, start_date: week, end_date: today}
            } else {
                dataParam = {...dataParam, start_date: month, end_date: today}
            }

            axios.get(`${process.env.REACT_APP_URL_PRODUCTS}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept'        : 'application/json',
                    'Authorization' : `${process.env.REACT_APP_AUTH}`
                }, 
                params: dataParam,
            }).then( res => {
                const result  = res.data.result;

                if(result.length === 0){
                    message.warning('data is empty');
                    setButtonLoading(false);
                    setHasContent(false);
                } else {

                    let companies = [];
                    let dataTable = [];
                    let columnNumber = {
                        title    : '',
                        dataIndex: 'no',
                        key      : 'no1',
                    }
    
                    // filter by category direct
                    const category = result.filter(val => val.category === 'Direct');
    
                    // filter by company;
                    const company     = groupByKey(category, 'company');
                    
                    // company keys
                    let   rowName = {}, rowGofood = {}, rowGrabfood = {}, rowOfficial = {}, rowFoodspot = {};
                    Object.keys(company).map((val, idx) => {
                        let   newCompanies           = {};
                              newCompanies.title     = val;
                              newCompanies.dataIndex = val;
                              newCompanies.key       = val;
    
                        if(val === newCompanies.title){
                            rowName.no   = 'name';
                            rowName.key  = 'name';
                            rowName[val] = company[val][0].name;
                        }
    
                        const source = groupByKey(company[val], 'source');
                        Object.keys(source).map( val2 => {
                            if(val2.toLowerCase() === 'gofood' && val === newCompanies.title){
                                const priceArr = source[val2].map( val2 => val2.price);
                                const average  = Math.round((priceArr.reduce((a, b) => a + b) / priceArr.length));
    
                                rowGofood.no   = 'gofood';
                                rowGofood.key  = 'gofood';
                                rowGofood[val] = average;
                            }
    
                            if(val2.toLowerCase() === 'grabfood' && val === newCompanies.title){
                                const priceArr = source[val2].map( val2 => val2.price);
                                const average  = Math.round((priceArr.reduce((a, b) => a + b) / priceArr.length));
    
                                rowGrabfood.no   = 'grabfood';
                                rowGrabfood.key  = 'grabfood';
                                rowGrabfood[val] =  average;
                            }
    
                            if(val2.toLowerCase() === 'foodspot' && val === newCompanies.title){
                                const priceArr = source[val2].map( val2 => val2.price);
                                const average  = Math.round((priceArr.reduce((a, b) => a + b) / priceArr.length));
    
                                rowFoodspot.no   = 'foodspot';
                                rowFoodspot.key  = 'foodspot';
                                rowFoodspot[val] = average;
                            }
    
                            if(val2.toLowerCase() === 'official site' && val === newCompanies.title){
                                const priceArr = source[val2].map( val2 => val2.price);
                                const average  = Math.round((priceArr.reduce((a, b) => a + b) / priceArr.length));
                                
                                rowOfficial.no   = 'official site';
                                rowOfficial.key  = 'official site';
                                rowOfficial[val] = average;
                            }
                            return ''
                            
                        })
                        companies.push(newCompanies);
                        return {...rowName, rowGofood, rowGrabfood, rowFoodspot, rowOfficial};
                    })
                    dataTable.push(rowName, rowGofood, rowGrabfood, rowOfficial,  rowFoodspot)
                    
                    // final check filtering not empty obj
                    const finalData = dataTable.map((val , idx) => {
                        val.key = idx;
                        return {...val};
                    }).filter(val => Object.keys(val).length > 0)
                    const filterData = finalData.map(val => {
                        delete val.key;
                        return val
                    }).filter(val2 => val2.no !== 'name' && Object.keys(val2).length > 0);
    
                    // modify array for graphic
                    let df = new DataFrame(filterData);
                        df = df.fillMissingValues(0);
                        df = df.transpose({transposeColumnNames:true});
                    let a  = df.toArray();
    
                    let data = []
                    for (let i = 1; i < a.length; i++){
                        let obj      = {}
                        obj['key']   = i
                        obj['name']  = a[i][3]
                        obj[a[0][0]] = a[i][0]
                        obj[a[0][1]] = a[i][1]
                        obj[a[0][2]] = a[i][2]
                        data.push(obj);
                    }
    
                    if(res.data.status === 200){
                        message.success('data is ready')
                        setTHCOL([columnNumber,...companies]);
                        setTableData(finalData);
                        setBarTable(data);
                        setHasContent(true);
                        setButtonLoading(false);
                    }
                }
                

            }).catch( err => message.error(err))
            
            return () => {
                isMount.current = false;
            }
        }
    }, [product, period, buttonLoading, groupByKey]);


    const changeProduct = (item) => {
        const prodSelect = products.filter((val, key) => key === item );
        setProduct(prodSelect[0]);
    }

    const changePeriod = (item) => {
        setPeriod(item );
    }

    const clickSubmit = (e) => {
        e.preventDefault();
        if(period !== '' && Object.keys(product).length > 0) setButtonLoading(true);
        else  message.error('make sure select the options');
    }

    
    return (
        <div className="centering-element shadow-xl text-3xl font-display">
            <h2 className="capitalize text-center text-3xl font-display font-semibold">Product Comparison</h2>
            <div className="group-select flex justify-center my-12">
                <Select 
                    defaultValue = "Product's Name"
                    style        = {{ width: 240 }}
                    className    = "mr-4 text-2xl"
                    onChange     = {(val) => changeProduct(val)}>
                        {products && products.map( (val,key) => {
                            return <Option value={key} key={key}>{val.name}</Option>
                        })}
                </Select>

                <Select 
                    defaultValue = "Select's Time"
                    style        = {{ width: 240 }}
                    className    = "mr-4 text-2xl"
                    onChange     = {(val) => changePeriod(val)}>
                        {periods && periods.map( (val,key) => {
                            return <Option value={val} key={key}>{val}</Option>
                        })}
                </Select>

                <Button 
                    type    = "primary"
                    onClick = {(e) => clickSubmit(e)}
                    loading = {buttonLoading}
                >submit</Button>
            </div>
            {hasContent ?
                <>
                    <Table className="mb-8" rowKey="key" dataSource={tableData} columns={THCOL} />
                    <BarChart width={730} height={350} data={barTable}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="gofood" fill={getRandomColor()} />
                        <Bar dataKey="grabfood" fill={getRandomColor()} />
                        <Bar dataKey="official site" fill={getRandomColor()} />
                        {/* <Bar dataKey="foodspot" fill={getRandomColor()} /> */}
                    </BarChart>
                </> : ''
            }

        </div>
    )
}
