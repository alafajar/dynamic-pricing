import { message } from 'antd';
import axios from 'axios';
import { useState, useEffect, useRef } from 'react';
import GraphicSales from '../Components/GraphicSales';
import UploadSales from '../Components/UploadSales';


const SalesData = ({groupByKey ,getRandomColor}) => {
    const isMount                   = useRef(true);
    const [dataRow, setDataRow]     = useState([]);
    const [dataTable, setDataTable] = useState([]);

    useEffect(() => {
        isMount.current = true;
        
        if(isMount.current){
            axios.get(`${process.env.REACT_APP_URL_SALES}`, {
                headers: {
                    'Content-Type' : 'application/json',
                    'Accept'       : 'application/json',
                    'Authorization': `${process.env.REACT_APP_AUTH}`
                }
            }).then( response => {
                const result = response.data.result;
                const status = response.data.status;
                if(status === 200){
                    setDataTable(result);
                }
            }).catch(err => {
                message.error(err)
            })
    
            return () => {
                isMount.current = false;
            }

        }
    }, [])
    
    return (
        <div className="centering-element shadow-xl text-3xl font-display">
            <UploadSales setRow={(val) => setDataRow(val)} dataTable={dataTable}  groupByKey={groupByKey}/>

            <hr className="my-20"/>
            
            <GraphicSales 
                dataRow        = {dataRow}
                dataTable      = {dataTable}
                groupByKey     = {groupByKey}
                getRandomColor = {getRandomColor} 
            />
        </div>
    )
}

export default SalesData
