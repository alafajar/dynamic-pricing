const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    minWidth: {
      '0': '0',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      'full': '100%',
    },
    fontFamily: {
      'display': ['Nunito', 'sans-serif'],
    },
    extend: {
      colors: {
        'dynamic-sky-1': '#e8ffff',
        'dynamic-sky-2': '#cffffe',
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
